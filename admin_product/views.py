from rest_framework import serializers, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404

from .serializers import *

class ViewsetProduct(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductViewSerializer

class FeaturedProductsList(APIView):
    def get(self, request, format =None):
        products = Product.objects.order_by(featured = True)
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)
    
class ProductDetail(APIView):
    def get_object(self, category_slug, product_slug):
        try:
            return Product.objects.filter(category__slug = category_slug).get(slug = product_slug)
        except Product.DoesNotExist:
            raise Http404
        
    def get(self, request, category_slug, product_slug, format = None):
        product = self.get_object(category_slug, product_slug)
        serializer = ProductSerializer(product)
        return Response(serializer.data)
    
class ViewsetCategory(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryViewSerializer

class CategoryDetail(APIView):
    def get_object(self, category_slug):
        try:
            return Category.objects.get(slug = category_slug)
        except Category.DoesNotExist:
            raise Http404
        
    def get(self, request, category_slug, format = None):
        category = self.get_object(category_slug)
        serializer = CategorySerializer(category)
        return Response(serializer.data)
    
class ViewsetProveedores(viewsets.ModelViewSet):
    queryset = Proveedores.objects.all()
    serializer_class = ProveedoresViewSerializer
    
class ViewsetEmpleados(viewsets.ModelViewSet):
    queryset = Empleados.objects.all()
    serializer_class = EmpleadosViewSerializer
    
class ViewsetApartado(viewsets.ModelViewSet):
    queryset = Apartado.objects.all()
    serializer_class = ApartadoViewSerializer
    
class ViewsetVenta(viewsets.ModelViewSet):
    queryset = Venta.objects.all()
    serializer_class = VentaViewSerializer
    
class ViewsetClientes(viewsets.ModelViewSet):
    queryset = Clientes.objects.all()
    serializer_class = ClientesViewSerializer