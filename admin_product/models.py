from django.db import models
from io import BytesIO #Libreria para manejo de Imagenes
from PIL import Image #Libreria para edicion de Imagenes
from django.core.files import File #Libreria para manejo de Archivo
from django.core.validators import MinValueValidator

class Proveedores(models.Model):
    name_company = models.CharField('Nombre de Compañia', max_length = 250)
    name_representative = models.CharField('Nombre de Representante', max_length = 250)
    address = models.CharField('Dirección', max_length = 400)
    phone = models.CharField('Telefono de Contacto', max_length = 10)
    
    def __str__(self):
        return self.name_company
class Empleados(models.Model):
    name = models.CharField('Nombre', max_length = 250)
    last_name = models.CharField('Apellido', max_length = 250)
    phone = models.CharField('Telefono', max_length = 10)
    address = models.CharField('Direccion Residencia', max_length = 400)
    date_admission = models.DateTimeField('Fecha de Ingreso')

    def __str__(self):
        return self.name
class Apartado(models.Model):
    empleado = models.ForeignKey(Empleados, on_delete=models.CASCADE)
    date_inicio = models.DateTimeField('Fecha de Inicio')
    date_fin = models.DateTimeField('Fecha Final')
    anticipo = models.BooleanField('Anticipo de Apartado')
    total = models.CharField('Total', max_length = 250)
    
    def __str__(self):
        return self.id
class Clientes(models.Model):
    address = models.CharField('Dirección', max_length = 400)
    name = models.CharField('Nombre de Cliente', max_length = 200)
    last_name = models.CharField('Apellido de Cliente', max_length = 200)
    phone = models.CharField('Telefono', max_length = 10)

    def __str__(self):
        return self.name
class Venta(models.Model):
    cliente = models.ForeignKey(Clientes, on_delete = models.CASCADE)
    empleado = models.ForeignKey(Empleados, on_delete = models.CASCADE)
    date = models.DateTimeField('Fecha de Venta')
    total = models.CharField('Total de Venta', max_length= 400)
    
    def __str__(self):
        return self.id

#Creacion de Modelo Categoria
class Category(models.Model):
    name = models.CharField('Nombre de Categoria', max_length = 255)
    slug = models.SlugField('Etiqueta') #Campo de etiqueta para categoria
    
    class Meta:
        ordering = ('name',)
    
    #Función de Formateo y muestra de Nombre Categoria
    def __str__(self):
        return self.name
    
    #Función para crear ruta de categoria
    def get_absolute_url(self):
        return f'/{self.slug}/'

#Creacion de Modelo Producto
class Product(models.Model):
    proveedor = models.ForeignKey(Proveedores, on_delete=models.CASCADE)
    exis_venta = models.BooleanField('Existe en Venta', default=False)
    exis_bodega = models.IntegerField('Existencia en Bodega', validators=[MinValueValidator(1)])
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField('Nombre de Producto', max_length = 255)
    slug = models.SlugField('Etiqueta') #Campo para etiqueta del producto
    description = models.TextField('Descripción', blank=True, null=True) #Campo para la descripcion del Producto
    price = models.DecimalField('Precio', max_digits=10, decimal_places=2) #Campo para precio del Producto
    date_added = models.DateTimeField('Fecha de Adición', auto_now_add=True) #Campo automatico para la fecha de ingreso del Producto
    featured = models.BooleanField()
    
    #Funcion para Ordenar los Productos por Fecha de Ingreso en forma Descendente
    class Meta:
        ordering = ('id',)
        
    def __str__(self):
        return self.name
    
    #Funcion para crear la url absoluta del producto partiendo de la categoria
    def get_absolute_url(self):
        return f'/{self.category.slug}/{self.slug}/'

    #Funcion para obtener url de la imagen del Producto
    def get_image(self):
        if self.image:
            return 'http://127.0.0.1:8000' + self.image.url
        else:
            return ''

    #Funcion para obtener url de la previsualizacion del Producto    
    def get_thumbnail(self):
        if self.thumbnail:
            return 'http://127.0.0.1:8000' + self.thumbnail.url
        else:
            if self.image:
                self.thumbnail = self.make_thumbnail(self.image)
                self.save()
                
                return 'http://127.0.0.1:8000' + self.thumbnail.url
            else: 
                return ''
    
    #Funcion para la edicion de tamaño de imagen original dirigida a la previsualizacion
    def make_thumbnail(self, image, size=(300, 200)):
        img = Image.open(image)
        img.convert('RGB')
        img.thumbnail(size)
        
        thumb_io = BytesIO()
        img.save(thumb_io, 'JPEG', quality=85)
        
        thumbnail = File(thumb_io, name=image.name)
        
        return thumbnail
    
    def NameCategory(self):
        return self.category.name