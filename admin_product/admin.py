from django.contrib import admin

from .models import *

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Proveedores)
admin.site.register(Empleados)
admin.site.register(Apartado)
admin.site.register(Venta)
admin.site.register(Clientes)