from rest_framework import serializers
from .models import *

class ProductViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            "id",
            "exis_venta",
            "exis_bodega",
            "proveedor",
            "category",
            "name",
            "slug",
            "description",
            "price",
            "date_added",
            "NameCategory",
            "featured",
        )
        
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "get_absolute_url",
            "description",
            "price",
        )

class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    class Meta:
        model = Category
        fields = (
            "id",
            "name",
            "slug",
            "get_absolute_url",
            "products",
        )
        
class CategoryViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            "id",
            "name",
            "slug",
        )
        
class ProveedoresViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedores
        fields = (
            "id",
            "name_company",
            "name_representative",
            "address",
            "phone",
        )
        
class EmpleadosViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleados
        fields = (
            "id",
            "name",
            "last_name",
            "phone",
            "address",
            "date_admission",
        )
        
class ApartadoViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Apartado
        fields = (
            "id",
            "empleado",
            "date_inicio",
            "date_fin",
            "anticipo",
            "total"
        )
        
class VentaViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venta
        fields = (
            "id",
            "empleado",
            "cliente",
            "date",
            "total"
        )
        
class ClientesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clientes
        fields = (
            "id",
            "address",
            "name",
            "last_name",
            "phone"
        )