from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import urls, routers, views
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from admin_product.views import *

apiurl = routers.SimpleRouter()
apiurl.register('product', ViewsetProduct)
apiurl.register('category', ViewsetCategory)
apiurl.register('proveedor', ViewsetProveedores)
apiurl.register('apartado', ViewsetApartado)
apiurl.register('venta', ViewsetVenta)
apiurl.register('empleado', ViewsetEmpleados)
apiurl.register('cliente', ViewsetClientes)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('admin_product.urls')),
    path('view/', include(apiurl.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)